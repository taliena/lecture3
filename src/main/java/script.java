import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;




public class script {
    public static void main(String[] args) throws InterruptedException {
        WebDriver driver = InitWebDriver();
        EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
        eventDriver.register(new WebEventListener());
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        Login("webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw", driver);
        WaitByLinkText("Каталог", driver);
        Actions builder = new Actions(driver);
        WebElement catalog = driver.findElement(By.linkText("Каталог"));
        builder.moveToElement(catalog).build().perform();
        WaitByLinkText("категории", driver);
        ClickElement("категории", driver);
        ClickElement("Добавить категорию", driver);
        WaitByXPath("//input[@id='name_1']", driver);
        CreateNewCategory("Dresses", driver);
        WaitByXPath("//button[@type='submit']", driver);
        ClickElementByXpath("//button[@type='submit']", driver);
        WaitByXPath("//*[@id='table-category']/thead/tr[1]/th[3]/span/a[1]", driver);
        ClickElementByXpath("//*[@id='table-category']/thead/tr[1]/th[3]/span/a[1]", driver);
        driver.quit();

    }


    public static WebDriver InitWebDriver() {

        String chromedriver = System.setProperty("webdriver.chrome.driver", Test.class.getResource("chromedriver").getPath());
        return new ChromeDriver();
    }

    public static void Login(String mail, String passwd, WebDriver driver) {
        WebElement email = driver.findElement(By.id("email"));
        email.sendKeys(mail);
        email.submit();
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys(passwd);
        password.submit();

    }

    public static void WaitByLinkText(String linkText, WebDriver driver) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.linkText(linkText)));

    }

    public static void WaitByXPath(String xpath, WebDriver driver) {
        (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

    }

    public static void ClickElement(String CategoryName, WebDriver driver) {
        WebElement categories = driver.findElement(By.linkText(CategoryName));
        categories.click();
    }

    public static void ClickElementByXpath(String xpath, WebDriver driver) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    public static void CreateNewCategory(String newCategory, WebDriver driver) {
        WebElement newElement = driver.findElement(By.xpath("//input[@id='name_1']"));
        newElement.sendKeys(newCategory);
    }

    public static class WebEventListener extends AbstractWebDriverEventListener {

        public void beforeNavigateTo(String url, WebDriver driver) {
            System.out.println("Before navigating to: '" + url + "'");
        }

        public void afterNavigateTo(String url, WebDriver driver) {
            System.out.println("Navigated to:'" + url + "'");
        }

        public void beforeClickOn(WebElement element, WebDriver driver) {
            System.out.println("Trying to click on: " + element.toString());
        }

        public void afterClickOn(WebElement element, WebDriver driver) {
            System.out.println("Clicked on: " + element.toString());
        }

        public void onException(Throwable error, WebDriver driver) {
            System.out.println("Error occurred: " + error);
        }
    }

}